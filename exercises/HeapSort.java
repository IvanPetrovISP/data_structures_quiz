package exercises;

import java.util.List;
import java.util.PriorityQueue;

public class HeapSort {

    public static <T> void heapSort(List<T> elements) {
        PriorityQueue<T> temp = new PriorityQueue<>(elements);
        elements.clear();
        while (temp.iterator().hasNext()) {
            elements.add(temp.poll());
        }
    }
}
