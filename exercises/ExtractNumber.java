package exercises;

public class ExtractNumber {

    public static int extractNumber(String text) {
        StringBuilder builder = new StringBuilder();

        int result = -1;

        outer:
        for (int i = 0; i < text.length(); i++) {
            if (text.charAt(i) >= '0' && text.charAt(i) <= '9') {
                builder.append(text.charAt(i));
                for (int j = i + 1; j < text.length(); j++) {
                    if (j == text.length() - 1) {
                        continue;
                    }
                    if (text.charAt(j) >= '0' && text.charAt(j) <= '9') {
                        builder.append(text.charAt(j));
                    } else {
                        result = Integer.parseInt(builder.toString());
                        break outer;
                    }

                }
            }
        }

        return result;
    }
}
