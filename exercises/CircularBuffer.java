package exercises;

import java.util.*;

public class CircularBuffer<T> {
    private int size;
    private Queue<T>  data;

    public CircularBuffer(int size) {
        this.size = size;
        this.data = new ArrayDeque<>();
    }

    @SafeVarargs
    public CircularBuffer(T... items) {
        this.size = items.length;
        this.data = new ArrayDeque<>();
        for (T item : items) {
            this.add(item);
        }
    }

    public boolean add(T element) {
        if (element == null) {
            return false;
        } else {
            if (this.data.size() == this.size) {
                this.data.poll();
                this.data.offer(element);
            } else {
                this.data.offer(element);
            }
            return true;
        }
    }

    public T remove() {
        if (this.data.size()!=0) {
            return this.data.poll();
        } else {
            return null;
        }
    }

    @Override
    public String toString() {
        return this.data.toString();
    }
}
