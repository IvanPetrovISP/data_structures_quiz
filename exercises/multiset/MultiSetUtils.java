package exercises.multiset;

import java.math.BigDecimal;
import java.util.Comparator;

public class MultiSetUtils<T extends Number> extends MultiSet<T> {

    public MultiSetUtils() {
        super();
    }

    /**
     * Returns the smallest element (key).
     *
     * @return
     */
    public T min() {
        return this.data.keySet().stream().sorted().limit(1).findFirst().orElse(null);
    }

    /**
     * Returns the largest element (key).
     *
     * @return
     */
    public T max() {
        return this.data.keySet().stream().sorted().skip(this.data.size() - 1).findFirst().orElse(null);
    }

    /**
     * Returns the smallest weighted element (weight is defined as key * value).
     *
     * @return
     */
    public T weightedMin() {
        return this.data.keySet().stream()
                .sorted(Comparator.
                        comparing(key -> new BigDecimal(key.toString()).
                                multiply(new BigDecimal(this.data.get(key).toString()))))
                .limit(1).findFirst().orElse(null);
    }

    /**
     * Returns the largest weighted element (weight is defined as key * value).
     *
     * @return
     */
    public T weightedMax() {
        return this.data.keySet().stream()
                .sorted((a, b) -> {
                    BigDecimal first = new BigDecimal(a.toString()).multiply(new BigDecimal(this.data.get(a).toString()));
                    BigDecimal second = new BigDecimal(b.toString()).multiply(new BigDecimal(this.data.get(b).toString()));
                    return second.compareTo(first);
                })
                .limit(1).findFirst().orElse(null);
    }

    /**
     * Returns the sum of all elements (keys).
     *
     * @return
     */
    public T sumElements() {
        return this.data.keySet().stream().reduce((T) BigDecimal.ZERO, (a, b) -> {
            BigDecimal first = new BigDecimal(a.toString());
            BigDecimal second = new BigDecimal(b.toString());
            return (T) first.add(second);
        });
    }

    //TODO - returns the weighted sum of the elements in the set (in case the element extends from Number)
    //I don't know how to proceed with this task.

    //TODO - concatenates the elements of two multisets
    //I think this is the same logic as addAll() method in the MultiSet.

    //TODO - returns a sorted list of the elements of the exercises.multiset
    //Again I don't know how to proceed here as I can't perform arithmetic operations on Number.
}
