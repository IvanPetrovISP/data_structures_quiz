package exercises.multiset;

import java.util.*;

public class MultiSet<T> implements Set<T> {
    private static final Integer INITIAL_COUNTER = 1;
    protected HashMap<T, Integer> data;

    public MultiSet() {
        this.data = new HashMap<>();
    }

    /**
     * @return The count of stored elements.
     */
    @Override
    public int size() {
        return this.data.size();
    }

    /**
     * @return The count of values(repetitions) of all stored elements.
     */
    public int weight() {
        return this.data.values().stream().mapToInt(Integer::intValue).sum();
    }

    /**
     * @return False if contains elements. True if empty.
     */
    @Override
    public boolean isEmpty() {
        return this.data.isEmpty();
    }

    /**
     * Iterator over elements.
     *
     * @return Iterator of type T.
     */
    @Override
    public Iterator<T> iterator() {
        return this.data.keySet().iterator();
    }

    /**
     * Checks if @o is present.
     *
     * @param o
     * @return True if present. False if not.
     */
    @Override
    public boolean contains(Object o) {
        return this.data.containsKey(o);
    }

    /**
     * Iterates over @c, calls @contains(Object o) on each element.
     *
     * @param c
     * @return Returns True if ALL elements in @c return True, otherwise False.
     */
    @Override
    public boolean containsAll(Collection<?> c) {
        if (c == null || c.isEmpty()) {
            return false;
        }

        for (Object o : c) {
            if (!this.contains(o)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Extracts all stored elements in an array. Each element is added as many times as it occurs in the MultiSet.
     *
     * @return Returns array Object[].
     */
    @Override
    public Object[] toArray() {
        Object[] result = new Object[this.weight()];

        int arrayCounter = 0;

        for (T t : this.data.keySet()) {
            int count = this.data.get(t);
            for (int i = 0; i < count; i++) {
                result[arrayCounter++] = t;
            }
        }

        return result;
    }

    /**
     * Calls @toArray()
     *
     * @param tArray
     * @param <T1>
     * @return Returns array of type T.
     */
    @Override
    public <T1> T1[] toArray(T1[] tArray) {
        Object[] objects = this.toArray();

        for (int i = 0; i < objects.length; i++) {
            tArray[i] = (T1) objects[i];
        }
        return tArray;
    }

    /**
     * Adds element @t to the MultiSet with default value(repetitions) of 1.
     * If @t is present, increment its value.
     *
     * @param t
     * @return True if successful, otherwise False.
     */
    @Override
    public boolean add(T t) {
        if (t == null) {
            return false;
        }

        if (this.contains(t)) {
            this.data.put(t, this.data.get(t) + 1);
        } else {
            this.data.put(t, INITIAL_COUNTER);
        }

        return true;
    }

    /**
     * Adds element @t to the MultiSet with @count value(repetitions).
     * If @t is present, increment its value by @count.
     *
     * @param t
     * @param count
     * @return True if successful, otherwise False.
     */
    public boolean add(T t, int count) {
        if (t == null || count < 1) {
            return false;
        }
        if (this.contains(t)) {
            this.data.put(t, this.data.get(t) + count);
        } else {
            this.data.put(t, count);
        }

        return true;
    }

    /**
     * Iterates over @c, calls @add(T t) on each element.
     *
     * @param c
     * @return Returns True if successful, otherwise False.
     */
    @Override
    public boolean addAll(Collection<? extends T> c) {
        if (c == null || c.isEmpty()) {
            return false;
        }
        for (T t : c) {
            this.add(t);
        }
        return true;
    }

    /**
     * Removes @o completely from the MultiSet.
     *
     * @param o
     * @return Returns True if successful, otherwise False.
     */
    @Override
    public boolean remove(Object o) {
        if (o == null || !this.contains(o)) {
            return false;
        }

        this.data.remove(o);
        return true;
    }

    /**
     * Iterates over @c, calls @remove(Object o) on each element.
     *
     * @param c
     * @return
     */
    @Override
    public boolean removeAll(Collection<?> c) {
        if (c == null || c.isEmpty()) {
            return false;
        }
        for (Object o : c) {
            this.remove(o);
        }
        return true;
    }

    /**
     * Decrements the value (repetitions) of @o by 1.
     * If value drops below 1, call @remove.
     *
     * @param o
     * @return Returns True if successful, otherwise False.
     */
    public boolean reduce(Object o) {
        if (o == null || !this.contains(o)) {
            return false;
        }

        if (this.data.get(o) == 1) {
            this.data.remove(o);
        } else {
            this.data.put((T) o, this.data.get(o) - 1);
        }
        return true;
    }

    /**
     * Iterates over all elements, calls @reduce(Object o) on each.
     *
     * @return
     */
    public boolean reduceAll() {
        this.data.keySet().forEach(this::reduce);
        return true;
    }

    /**
     * Iterates over @c, calls @reduce(Object o) on each element.
     *
     * @param c
     * @return
     */
    public boolean reduceAll(Collection<?> c) {
        if (c == null || c.isEmpty()) {
            return false;
        }
        for (Object o : c) {
            this.reduce(o);
        }
        return true;
    }

    /**
     * Iterates over @c and calls Map.keySet().retainAll().
     *
     * @param c
     * @return
     */
    @Override
    public boolean retainAll(Collection<?> c) {
        if (c == null || c.isEmpty()) {
            return false;
        }

        this.data.keySet().retainAll(c);

        return true;
    }

    /**
     * Removes all stored elements.
     */
    @Override
    public void clear() {
        this.data.clear();
    }

    /**
     * Returns a String which represents each element with the number of stored repetitions in the format: Element x Repetitions.
     * If an element is present only once (no repetitions), the "x Repetitions" part is ignored.
     * Example: [Dog x 3, Cat, Cow x 2]
     *
     * @return
     */
    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("[");

        for (T t : data.keySet()) {
            if (this.data.get(t) == 1) {
                result.append(t);
            } else {
                result.append(t).append(" x ").append(this.data.get(t));
            }
            result.append(", ");
        }

        result.setLength(result.length() - 2);
        result.append("]");
        return result.toString();
    }

}