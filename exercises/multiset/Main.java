package exercises.multiset;

import java.util.*;

public class Main {
    public static void main(String[] args) {

        MultiSet<String> multiSet = new MultiSet<>();

        System.out.println(String.format("Empty: %b%n", multiSet.isEmpty()));

        multiSet.add("Ivan");
        multiSet.add("Zhelyazko");
        multiSet.add("Ani");
        multiSet.add("Veso");

        System.out.println(String.format("Empty: %b", multiSet.isEmpty()));
        System.out.println(String.format("Size: %d\t\tWeight: %d", multiSet.size(), multiSet.weight()));
        System.out.println(multiSet.toString());
        System.out.println();

        multiSet.add("Sasho", 1);
        multiSet.add("Rado", 2);
        multiSet.add("INVALID", 0);

        System.out.println(String.format("Size: %d\t\tWeight: %d", multiSet.size(), multiSet.weight()));
        System.out.println(multiSet.toString());
        System.out.println();

        List<String> list = new ArrayList<>();
        list.add("Pesho");
        list.add("Ico");
        list.add("Emo");

        multiSet.addAll(list);
        multiSet.addAll(list);

        System.out.println(String.format("Size: %d\t\tWeight: %d", multiSet.size(), multiSet.weight()));
        System.out.println(multiSet.toString());
        System.out.println();

        multiSet.reduce("Pesho");

        System.out.println(String.format("Size: %d\t\tWeight: %d", multiSet.size(), multiSet.weight()));
        System.out.println(multiSet.toString());
        System.out.println();

        multiSet.reduce("Pesho");

        System.out.println(String.format("Size: %d\t\tWeight: %d", multiSet.size(), multiSet.weight()));
        System.out.println(multiSet.toString());
        System.out.println();

        multiSet.remove("Ico");

        System.out.println(String.format("Size: %d\t\tWeight: %d", multiSet.size(), multiSet.weight()));
        System.out.println(multiSet.toString());
        System.out.println();

        multiSet.reduceAll(list);

        System.out.println(String.format("Size: %d\t\tWeight: %d", multiSet.size(), multiSet.weight()));
        System.out.println(multiSet.toString());
        System.out.println();

        Object[] objects = multiSet.toArray();
        System.out.println(Arrays.toString(objects));
        String[] strings = multiSet.toArray(new String[multiSet.weight()]);
        System.out.println(Arrays.toString(strings));

//        MultiSetUtils<Double> nums = new MultiSetUtils<>();
//        nums.add(5.0);
//        nums.add(3.0);
//        nums.add(3.0);
//        nums.add(4.0);
//        nums.add(4.0);

//        double smallestWeight = nums.weightedMin();
//        double largestWeight = nums.weightedMax();
//
//        double sum = nums.sumElements();
////        double wsum = nums.sumWeightedElements();
//        System.out.println(sum);
//        System.out.println(wsum);
//
//        System.out.println(nums.min());
//        System.out.println(nums.max());
//        System.out.println(smallestWeight);
//        System.out.println(largestWeight);
    }
}
