package exercises;

import java.util.LinkedHashMap;
import java.util.Map;

public class SortByValue {

    public static void sortByValue(LinkedHashMap<String, String> elements) {

        LinkedHashMap<String, String> sort = new LinkedHashMap<>();
        elements.
                entrySet().stream().
                sorted(Map.Entry.comparingByValue()).
                forEach(e -> sort.put(e.getKey(), e.getValue()));

        elements.clear();
        elements.putAll(sort);
    }
}
