package exercises;

public class DumpText {

    public static void dumpText(String text) {
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < text.length(); i++) {
            if (i == text.length() - 1) {
                result.append(text.charAt(i));
            } else if (text.charAt(i) != text.charAt(i + 1)) {
                result.append(text.charAt(i));
            }
        }

        System.out.println(result.toString());
    }
}
