package exercises.timus;

import java.io.*;
import java.util.*;

public class FinalStandings {
    public static void main(String[] args) throws IOException {
        StreamTokenizer tokenizer = new StreamTokenizer(new BufferedReader(new InputStreamReader(System.in)));
        PrintWriter writer = new PrintWriter(new OutputStreamWriter(System.out));

        tokenizer.nextToken();
        int num = (int) tokenizer.nval;
        if (num < 1 || num > 150000) {
            return;
        }

        Map<Integer, Integer> map = new LinkedHashMap<>();

        for (int i = 0; i < num; i++) {
            tokenizer.nextToken();
            int k = (int) tokenizer.nval;
            tokenizer.nextToken();
            int v = (int) tokenizer.nval;
            map.put(k, v);
        }

        map.entrySet().stream().sorted(Map.Entry.comparingByValue(Collections.reverseOrder()))
                .forEach(e -> writer.println(e.getKey() + " " + e.getValue()));
        writer.println();
        writer.flush();
        writer.flush();
        writer.close();
    }

}
