package exercises.timus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Palindrome {
    public Palindrome() {
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        System.out.println(getPalindrome(input));
    }

    public static String getPalindrome(String input) {
        String result = "";
        if (isNotValid(input)) {
            return result;
        }

        ArrayList<String> list = new ArrayList<>();
        int currentMax = 0;

        for (int i = 0; i < input.length(); i++) {
            for (int j = i; j < input.length(); j++) {
                String temp = input.substring(i, j + 1);
                if (temp.length() >= currentMax) {
                    if (isPalindrome(temp)) {
                        list.add(temp);
                        currentMax = temp.length();
                    }
                }
            }
        }

        return list.stream().
                sorted((a, b) -> b.length() - a.length()).
                limit(1).
                findFirst().orElse(null);
    }

    private static boolean isNotValid(String input) {
        return input == null || input.isEmpty();
    }

    private static boolean isPalindrome(String input) {
        char[] natural = input.toCharArray();
        char[] reversed = new char[natural.length];

        for (int i = 0; i < natural.length; i++) {
            reversed[i] = natural[natural.length - 1 - i];
        }

        return Arrays.equals(natural, reversed);
    }

}
