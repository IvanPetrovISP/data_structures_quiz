package exercises.timus;

import java.io.*;
import java.util.*;

public class DistanceTree {
    private static PrintWriter writer = new PrintWriter(new OutputStreamWriter(System.out));
    private static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private static Map<Integer, Node> nodes = new HashMap<>();
    private static Map<Integer, Integer> links = new HashMap<>();
    private static Map<Integer, int[]> tokens = new HashMap<>();
    private static int[][] queries;

    public static void main(String[] args) throws IOException {

        int n = Integer.parseInt(reader.readLine());

        if (n == 1) {
            writer.println(0);
            writer.flush();
            writer.close();
            return;
        }

        storeNodesInformation(n);

        int m = Integer.parseInt(reader.readLine());

        queries = new int[m][2];

        storeQueriesInformation(m);

        reader.close();

        initNodes(n);

        callQueries(m);

        writer.flush();
        writer.close();
    }

    /**
     * Executes all the queries and stores their result in the manner they were read from the console.
     *
     * @param m the count of queries.
     */
    private static void callQueries(int m) {
        for (int[] query : queries) {
            writer.println(calculateWeight(query[0], query[1]));
        }
    }

    /**
     * Stores all the information about the queries for later use with the purpose to optimize the whole program.
     * All queries will be executed once the whole reading completes.
     *
     * @param m the count of queries.
     * @throws IOException
     */
    private static void storeQueriesInformation(int m) throws IOException {
        for (int i = 0; i < m; i++) {
            String[] nums = reader.readLine().split("\\s+");
            queries[i][0] = Integer.parseInt(nums[0]);
            queries[i][1] = Integer.parseInt(nums[1]);
        }
    }

    /**
     * Initializes all the Nodes with the according stored information about them.
     * Since any Node can be the Root node, once is defined as such before the rest by extracting the Node with
     * most links towards it.
     * Then until all the Nodes are initialized, a new Node is being initialized if and only if it's previous Node
     * is already existing. This helps for smooth linking between the Nodes without having gaps.
     *
     * @param n the count of Nodes.
     */
    private static void initNodes(int n) {
        int rootNode = links.keySet().stream()
                .sorted((a, b) -> links.get(b).compareTo(links.get(a)))
                .limit(1).findFirst().orElse(0);

        nodes.put(rootNode, new Node(rootNode, -1, 0));
        tokens.remove(rootNode);

        while (nodes.size() < n) {
            int current = -1;
            for (Map.Entry<Integer, int[]> entry : tokens.entrySet()) {
                if (nodes.containsKey(entry.getValue()[1])) {
                    Node node = new Node(entry.getValue()[0], entry.getValue()[1], entry.getValue()[2]);
                    node.setNodes(nodes.get(entry.getValue()[1]).getNodes());
                    nodes.put(entry.getKey(), node);
                    current = entry.getKey();
                    break;
                }
            }
            tokens.remove(current);
        }
    }

    /**
     * Stores all the information about the Nodes for later use with the purpose to optimize the whole program.
     * All Nodes will be initialized once the whole reading completes.
     *
     * @param n the count of Nodes.
     * @throws IOException
     */
    private static void storeNodesInformation(int n) throws IOException {

        for (int i = 0; i < n - 1; i++) {
            String[] nums = reader.readLine().split("\\s+");
            int id = Integer.parseInt(nums[0]);
            int previous = Integer.parseInt(nums[1]);
            int weight = Integer.parseInt(nums[2]);

            if (!links.containsKey(id)) {
                links.put(id, 1);
            } else {
                links.put(id, links.get(id) + 1);
            }

            if (!links.containsKey(previous)) {
                links.put(previous, 1);
            } else {
                links.put(previous, links.get(previous) + 1);
            }

            if (links.containsKey(id)) {
                tokens.putIfAbsent(id, new int[]{id, previous, weight});
            }

            if (links.containsKey(previous)) {
                tokens.putIfAbsent(previous, new int[]{previous, id, weight});
            }
        }

    }

    /**
     * Calculates the weight between the two Nodes by finding the shortest route that links them together.
     * First step is to even the path towards the Root Node of both Nodes.
     * Second step is to search for the first common Node.
     * In both steps the weight of each Node on their way towards the first common Node or the Root Node is summed
     * and then later returned.
     *
     * @param one the number of the first Node.
     * @param two the number of the second Node.
     * @return the weight from the shortest route between the two Nodes.
     */
    private static int calculateWeight(int one, int two) {
        ArrayDeque<Integer> oneNodes = nodes.get(one).getNodes().clone();
        ArrayDeque<Integer> twoNodes = nodes.get(two).getNodes().clone();

        int weight = 0;

        while (oneNodes.size() > twoNodes.size()) {
            weight += nodes.get(oneNodes.pop()).getWeight();
        }
        while (twoNodes.size() > oneNodes.size()) {
            weight += nodes.get(twoNodes.pop()).getWeight();
        }

        int oneRoot = oneNodes.size() == 0 ? 0 : oneNodes.peek();
        int twoRoot = twoNodes.size() == 0 ? 0 : twoNodes.peek();

        while (oneRoot != twoRoot) {
            weight += nodes.get(oneNodes.pop()).getWeight();
            oneRoot = oneNodes.peek();
            weight += nodes.get(twoNodes.pop()).getWeight();
            twoRoot = twoNodes.peek();
        }

        return weight;
    }

    /**
     * Custom Node class to store it's information.
     * id - the number of the Node.
     * previous -  the number of the Node before it (in terms of going from the current Node towards the Root).
     * weight - the weight between the latter two.
     */
    private static class Node {
        private int id;
        private int previous;
        private int weight;
        private ArrayDeque<Integer> nodes;

        public Node(int id, int previous, int weight) {
            this.id = id;
            this.previous = previous;
            this.weight = weight;
            this.nodes = new ArrayDeque<>();
        }

        public int getWeight() {
            return this.weight;
        }

        public ArrayDeque<Integer> getNodes() {
            return this.nodes;
        }

        /**
         * When initializing a Node, it assumes the already established path towards the Root from it's previous.
         *
         * @param nodes The nodes collection of the previous Node.
         */
        public void setNodes(ArrayDeque<Integer> nodes) {
            this.nodes = nodes.clone();
            if (!this.nodes.contains(previous)) {
                this.nodes.push(previous);
            }
            this.nodes.push(id);
        }
    }
}
