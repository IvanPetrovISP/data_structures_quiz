package exercises.timus;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SandrosBook {
    public SandrosBook() {
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        System.out.println(getMostPowerfulSpell(input));
    }

    public static String getMostPowerfulSpell(String input) {
        TreeMap<Integer, String> spells = new TreeMap<>(Collections.reverseOrder());

        for (int i = 0; i < input.length(); i++) {
            for (int j = i; j < input.length(); j++) {
                String spellRegex = input.substring(i, j + 1);

                Pattern pattern = Pattern.compile(spellRegex);
                Matcher matcher = pattern.matcher(input);

                int currentPower = 0;
                while (matcher.find()) {
                    currentPower++;
                }

                spells.put(currentPower, spellRegex);
            }
        }

        return spells.values().stream().limit(1).findFirst().orElse(null);
    }

}
