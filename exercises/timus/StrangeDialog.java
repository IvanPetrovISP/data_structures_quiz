package exercises.timus;

import java.io.*;

public class StrangeDialog {
    private static final String YES = "YES";
    private static final String NO = "NO";
    private static final String REGEX = "^(outputone|outputon|inputone|inputon|output|out|puton|input|in|one)+$";

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in), 2 * 1024);
        PrintWriter writer = new PrintWriter(new OutputStreamWriter(System.out));
        int count = Integer.parseInt(reader.readLine());

        for (int i = 0; i < count; i++) {
            writer.println(reader.readLine().
                    matches(REGEX) ? YES : NO);
        }
        reader.close();
        writer.flush();
        writer.close();
    }

}