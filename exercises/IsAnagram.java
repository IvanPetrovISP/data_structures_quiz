package exercises;

import java.util.Arrays;

public class IsAnagram {

    public static boolean isAnagram(String first, String second) {
        char[] tempFirst = first.replaceAll("\\s", "").toLowerCase().toCharArray();
        char[] tempSecond = second.replaceAll("\\s", "").toLowerCase().toCharArray();

        Arrays.sort(tempFirst);
        Arrays.sort(tempSecond);

        if (tempFirst.length != tempSecond.length) {
            return false;
        }

        return Arrays.equals(tempFirst, tempSecond);
    }
}
