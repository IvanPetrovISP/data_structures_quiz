import exercises.*;

import java.util.*;

public class Main {
    public static void main(String[] args) {

        callExtractNumber(); // 1 extractNumber
        callIsAnagram(); // 2 isAnagram
        dumpText(); // 3 dumpText
        callDuplicateString(); // 4 duplicateString
        callSortByValue(); // 5 sortByValue
        callHeapSort(); // 6 heapSort
        callCircularBuffer(); // 9 ringBuffer / circularBuffer

    }

    private static void dumpText() {
        System.out.println("- DUMP TEXT -");
        String text = "aaabbbacaa";
        System.out.print(String.format("%s -> ", text));
        DumpText.dumpText(text);
        System.out.println();
    }

    private static void callCircularBuffer() {
        System.out.println("- CIRCULAR BUFFER -");
        CircularBuffer<Integer> circularBuffer = new CircularBuffer<>(5);
        circularBuffer.add(1);
        circularBuffer.add(2);
        circularBuffer.add(3);
        circularBuffer.add(4);
        circularBuffer.add(5);
        System.out.println("Size 5.");
        System.out.print("Adding 5 elements -> ");
        System.out.println(circularBuffer.toString());
        circularBuffer.remove();
        circularBuffer.remove();
        System.out.print("Removing 2 elements -> ");
        System.out.println(circularBuffer.toString());
        circularBuffer.add(6);
        circularBuffer.add(7);
        System.out.print("Adding 2 elements -> ");
        System.out.println(circularBuffer.toString());
        circularBuffer.add(8);
        circularBuffer.add(9);
        System.out.print("Adding 2 elements -> ");
        System.out.println(circularBuffer.toString());
    }

    public static void callHeapSort() {
        System.out.println("- HEAP SORT -");
        List<Integer> heap = new ArrayList<>();
        heap.add(1);
        heap.add(17);
        heap.add(3);
        heap.add(90);
        heap.add(101);
        heap.add(-1);
        heap.add(10);
        System.out.print("Before: ");
        System.out.println(heap);
        HeapSort.heapSort(heap);
        System.out.print("After: ");
        System.out.println(heap);
        System.out.println();
    }

    public static void callExtractNumber() {
        System.out.println("- EXTRACT NUMBER -");
        String input = "asd123asd";
        System.out.printf("%s -> %d%n", input, ExtractNumber.extractNumber(input));
        System.out.println();
    }

    public static void callIsAnagram() {
        System.out.println("- IS ANAGRAM -");
        System.out.printf("iVaN & nAvI -> %b%n", IsAnagram.isAnagram("iVaN", "nAvI"));
        System.out.printf("True & False -> %b%n", IsAnagram.isAnagram("True", "False"));
        System.out.println();
    }

    public static void dumpText(String text) {
        System.out.println("- DUMP TEXT -");
        Set<Character> set = new LinkedHashSet<>();
        StringBuilder result = new StringBuilder();
        for (Character c : text.toCharArray()) {
            if (set.add(c)) {
                result.append(c);
            }
        }

        System.out.println(result.toString());
        System.out.println();
    }

    public static void callDuplicateString() {
        System.out.println("- DUPLICATE TEXT -");
        String input = "Ivan";
        System.out.printf("%s -> %s%n", input, DuplicateString.duplicateString(input));
        System.out.println();
    }

    public static void callSortByValue() {
        System.out.println("- SORT BY VALUE -");
        LinkedHashMap<String, String> map = new LinkedHashMap<>();
        map.put("a", "q");
        map.put("b", "h");
        map.put("c", "z");
        map.put("d", "r");
        map.put("e", "r");
        System.out.print("Before:\t");
        System.out.println(map.toString());
        SortByValue.sortByValue(map);
        System.out.print("After:\t");
        System.out.println(map.toString());
        System.out.println();
    }

}
